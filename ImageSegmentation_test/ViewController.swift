//
//  ViewController.swift
//  ImageSegmentation_test
//
//  Created by Salvatore  Polito on 21/04/2019.
//  Copyright © 2019 Salvatore  Polito. All rights reserved.
//

import UIKit
import Fritz
import MetalPetal
import GPUImage


class ViewController: UIViewController {
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var imageView_background: UIImageView!
    
    var orig : UIImage = #imageLiteral(resourceName: "IMG_4614") {
        didSet {
            output = makeBokehImage(image: orig, radius: 20)!
        }
    }
    
    var output : UIImage? {
        didSet{
            imageView_background.image = output
            saveButton.isHidden = output == nil
            cancelButton.isHidden = output == nil
        }
    }
    
    lazy var model = FritzVisionPeopleSegmentationModel()
    
    let picker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        saveButton.isHidden = true
        cancelButton.isHidden = true
        imageView_background.isHidden = true
        
        
        
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.delegate = self
    }
    
    
    @IBAction func saveButtonTap(_ sender: Any) {
        let img = output!
        UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil)
        
        imageView_background.isHidden = true
        cancelButton.isHidden = true
        selectButton.isHidden = false
    }
    
    
    @IBAction func selectButtonTap(_ sender: Any) {
        present(picker, animated: true, completion: nil)
    }
    
    @IBAction func cancelButtonTap(_ sender: Any) {
        saveButton.isHidden = true
        cancelButton.isHidden = true
        imageView_background.isHidden = true
        
        selectButton.isHidden = false
    }
    
    
}

extension ViewController {
    func edgeMask(mask: FritzVisionSegmentationResult, origCIImage: CIImage) -> MTIImage? {
        let contextOptions = MTIContextOptions()
        
        return
        autoreleasepool{ () -> MTIImage? in
            guard let device = MTLCreateSystemDefaultDevice(), let context = try? MTIContext(device: device, options: contextOptions) else {return nil}
            
            let sharpnedOrigImage = origCIImage.applyingFilter("CIUnsharpMask", parameters: ["inputIntensity" : 2, "inputRadius" : 3.0])
            let refinedOrigImage = sharpnedOrigImage.applyingFilter("CIPhotoEffectMono")
            
            let blendWithMaskFilter = MTIBlendWithMaskFilter()
            let blur = MTIMPSGaussianBlurFilter()
            let clearMTIImage = MTIImage(color: .clear, sRGB: true, size: origCIImage.extent.size)
            
            
            //First mask
            let maskImage = mask.toImageMask(of: FritzVisionPeopleClass.person, threshold: 0.9, alpha: 255, minThresholdAccepted: 0.6)!
            
            //Blurring first mask to smooth edges
            blur.inputImage = MTIImage(cgImage: maskImage.cgImage!).unpremultiplyingAlpha()
            blur.radius = 10
            
            //First mask ready
            let firstMaskBlurred = blur.outputImage!
            
        
            /// 1
            //Cutting people from original image using blurred mask using blend with mask
            blendWithMaskFilter.inputBackgroundImage = clearMTIImage
            blendWithMaskFilter.inputMask = MTIMask(content: firstMaskBlurred, component: .alpha, mode: .normal)
            blendWithMaskFilter.inputImage = MTIImage(ciImage: refinedOrigImage).unpremultiplyingAlpha()
            
            guard let peopleCGImage = try? context.makeCGImage(from: blendWithMaskFilter.outputImage!) else {return nil}
            var peopleOutput : UIImage? = UIImage(cgImage: peopleCGImage)
            
            blur.inputImage = nil
            blendWithMaskFilter.inputImage = nil
            blendWithMaskFilter.inputMask = nil
            blendWithMaskFilter.inputBackgroundImage = nil
            
            
            let sobelOperator = ThresholdSobelEdgeDetection_White()
            sobelOperator.edgeStrength = 0.1
            sobelOperator.threshold = 10
            
            let edges = peopleOutput!.filterWithOperation(sobelOperator)
            
            peopleOutput = nil
            
            
            //We invert the result of model prediction
            //This time we need more stingent area because later we will add external edges
            let secondMask = mask.toImageMask(of: FritzVisionPeopleClass.person, threshold: 0.3, alpha: 255, minThresholdAccepted: 0.7)!
            let secondMaskMTIImage = MTIImage(cgImage: secondMask.cgImage!).unpremultiplyingAlpha()
            
            //Also we smooth corners of the second mask using blur filter
            blur.inputImage = secondMaskMTIImage
            blur.radius = 25
            
            //Second mask ready
            let secondMaskBlurred = blur.outputImage!
            
            //        let ooooo4o = try! context.makeCGImage(from: blur.outputImage!)
            //        let ooooooo4 = UIImage(cgImage: ooooo4o)
            
            /// 3
            //Now we blend the result of sobel with the result of the second mask
            let edgeBlend = MTIBlendFilter(blendMode: .hardLight)
            edgeBlend.inputImage = MTIImage(cgImage: edges.cgImage!).unpremultiplyingAlpha()
            edgeBlend.inputBackgroundImage = secondMaskBlurred
        
            
            return edgeBlend.outputImage!
        }
    }
    
    
    func bokehImage(mask: MTIImage, origImage: MTIImage, radius: Float, angle: Float = 3) -> UIImage? {
        let contextOptions = MTIContextOptions()
        
        return
        autoreleasepool{ () -> UIImage? in
            guard let device = MTLCreateSystemDefaultDevice(), let context = try? MTIContext(device: device, options: contextOptions) else {return nil}
            
            let hexagonBokehFilter = MTIHexagonalBokehBlurFilter()
            hexagonBokehFilter.angle = 3
            hexagonBokehFilter.radius = radius
            
            let finalMask = MTIMask(content: mask, component: .alpha, mode: .oneMinusMaskValue)
            hexagonBokehFilter.inputImage = origImage
            hexagonBokehFilter.inputMask = finalMask

            guard let cgImage = try? context.makeCGImage(from: hexagonBokehFilter.outputImage!.unpremultiplyingAlpha()) else {return nil}
            
            return UIImage(cgImage: cgImage)
        }
    }
    
    
    func makeBokehImage(image: UIImage, radius: Float = 10) -> UIImage? {
        
        let origCIimage = CIImage(image: image)!
        let origMTIImage = MTIImage(ciImage: origCIimage).unpremultiplyingAlpha()
        
        let sharpnedOrigImage = origCIimage.applyingFilter("CIUnsharpMask", parameters: ["inputIntensity" : 2, "inputRadius" : 3.0])
        let refinedOrigImage = sharpnedOrigImage.applyingFilter("CIPhotoEffectMono")
        
        
        
        let modelInput = FritzVisionImage(image: refinedOrigImage.toUIImage())

        let options = FritzVisionSegmentationModelOptions()
        options.imageCropAndScaleOption = .scaleFit
        
        guard let mask = try? model.predict(modelInput, options: options) else {return nil}

        
        let finalMask = edgeMask(mask: mask, origCIImage: origCIimage)
        
        
        return bokehImage(mask: finalMask!, origImage: origMTIImage, radius: radius)
    }
    

}


extension ViewController : UIImagePickerControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imageView_background.isHidden = true
        cancelButton.isHidden = true
        selectButton.isHidden = false
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imageView_background.isHidden = false
        saveButton.isHidden = false
        cancelButton.isHidden = false
        
        dismiss(animated: true, completion: nil)
        
        
        if let img = info[.originalImage] as? UIImage {
            orig = img.fixedOrientation()!
        }
    }
}

extension ViewController : UINavigationControllerDelegate {
    
}


extension UIImage {
    
    func fixedOrientation() -> UIImage? {
        
        guard imageOrientation != UIImage.Orientation.up else {
            //This is default orientation, don't need to do anything
            return self.copy() as? UIImage
        }
        
        guard let cgImage = self.cgImage else {
            //CGImage is not available
            return nil
        }
        
        guard let colorSpace = cgImage.colorSpace, let ctx = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue) else {
            return nil //Not able to create CGContext
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat.pi)
            break
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat.pi / 2.0)
            break
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: CGFloat.pi / -2.0)
            break
        case .up, .upMirrored:
            break
        }
        
        //Flip image one more time if needed to, this is to prevent flipped image
        switch imageOrientation {
        case .upMirrored, .downMirrored:
            transform.translatedBy(x: size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case .leftMirrored, .rightMirrored:
            transform.translatedBy(x: size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case .up, .down, .left, .right:
            break
        }
        
        ctx.concatenate(transform)
        
        switch imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
        default:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            break
        }
        
        guard let newCGImage = ctx.makeImage() else { return nil }
        return UIImage.init(cgImage: newCGImage, scale: 1, orientation: .up)
    }
}

extension UIImage {
    func resample(withScale scale: CGFloat) -> UIImage? {
        guard let cgImage = self.cgImage else {print("Couldn't extract cgImage - resample"); return nil}
        
        var srcBuffer : vImage_Buffer = vImage_Buffer()
        
        var format = vImage_CGImageFormat(
            bitsPerComponent: 8,
            bitsPerPixel: 32,
            colorSpace: nil,
            bitmapInfo: CGBitmapInfo(
                rawValue: CGImageAlphaInfo.premultipliedFirst.rawValue),
            version: 0,
            decode: nil,
            renderingIntent: CGColorRenderingIntent.defaultIntent)
        
        let error = vImageBuffer_InitWithCGImage(&srcBuffer, &format, nil, cgImage, UInt32(kvImageNoFlags))
        
        if error != kvImageNoError {
            free(srcBuffer.data)
            print("error")
            return nil
        }
        
        let dstWidth = Int(CGFloat(cgImage.width) * scale)
        let dstHeight = Int(CGFloat(cgImage.height) * scale)
        let bytesPerPixel = 4
        let dstBytesPerRow = bytesPerPixel * dstWidth
        
        var dstData = calloc(dstHeight * dstWidth * bytesPerPixel, 1)
        
        var dstBuffer = vImage_Buffer(data: dstData, height: UInt(dstHeight), width: UInt(dstWidth), rowBytes: dstBytesPerRow)
        
        var scaleError = vImageScale_ARGB8888(&srcBuffer, &dstBuffer, nil, vImage_Flags(kvImageHighQualityResampling))
        
        if scaleError != kvImageNoError {
            free(&dstData)
            print("error")
            return nil
        }
        
        free(srcBuffer.data)
        
        scaleError = kvImageNoError
        let scaledCGImage = vImageCreateCGImageFromBuffer(&dstBuffer, &format, nil, nil, vImage_Flags(kvImageNoFlags), &scaleError)
        
        free(dstBuffer.data)
        
        let image = UIImage(cgImage: scaledCGImage!.takeRetainedValue())
        
        
        return image
    }
}


extension CIImage {
    func toUIImage() -> UIImage {
        return
        autoreleasepool { () -> UIImage in
            let context:CIContext = CIContext.init(options: nil)
            let cgImage:CGImage = context.createCGImage(self, from: self.extent)!
            
            return UIImage.init(cgImage: cgImage)
        }
    }

}
